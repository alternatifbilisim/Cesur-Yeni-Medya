#Cesur Yeni Medya
Alternatif Bilişim tarafından derlenen elektronik kitap __Cesur Yeni Medya__, üç bölümden oluşuyor.

__İlden Dirini__'nin giriş ve değerlendirme yazısının devamında başlayan birinci bölüm, Tunus'ta başlayıp Mısır'a ve diğer birçok Arap ülkesine sıçrayan ve Libya'daki gelişmelerle süregiden Arap isyanlarında yeni medyanın oynadığı rolü tartışıyor. Konuyu farklı açılardan ele alan __İsmail Hakkı Polat, Ayşe Kaymak, Barış Engin, Koray Löker__ ve __Işık Barış Fidaner__'in yazılarını içeren bölümdeki tartışmalar, __Alternatif Bilişim__'in Ocak ayında Elektrik Mühendisleri Odası'nda düzenlediği Wikileaks konulu toplantıda ortaya atılan konuların genişletilmesi ile oluştu.

İkinci bölüm, Wikileaks'i gazetecilik ve yeni medya bağlamında inceliyor. __Özgür Uçkan, Mete Çubukçu, Noyan Ayan, Başar Başaran ve Gülseren Adaklı__'nın yazılarının yer aldığı bu bölüm, Wikileaks ve sızıntı gazeteciliğinin toplumsal bilgi ve iktidar değişimi açısından sağladığı olanaklar, siyasal konumlanışı gibi tartışmaları içeriyor.

Üçüncü bölümde, Wikileaks'i oluşum halindeki ve sonuçlarına götürülen bir fikir olarak anlamaya çalışıyoruz. __Julian Assange__'dan çevirdiğimiz üç yazının ardından __Işık Barış Fidaner, Burçe Çelik__ ve __Andreas Müllerleile__'nin yazıları Wikileaks'in fikirsel temelleri, imkanları ve sonuçlarını ele alıyor.

__Cesur Yeni Medya__  
*Wikileaks ve 2011 Arap isyanları üzerine tartışmalar*  
Derleme, Nisan 2011, 164 Sayfa  
__Derleyen__: Mutlu Binark, Işık Barış Fidaner  
__Yayına hazırlayan__: Işık Barış Fidaner  
__Kapak tasarımı__: Babür Sağlam  
Yazıların hakları yazarlara aittir.  
Kitabın LaTeX kodları CC Attribution-NonCommercial 3.0 Unported License altındadır.  
##İçindekiler

Giriş ve Değerlendirme / __İlden Dirini__  

###Bölüm I: Hak ve Özgürlükler, Arap isyanları

    Cesur Yeni Medya / İsmail Hakkı Polat  
    Wikileaks Okumak Suç mu? Assange Terörist mi? / Ayşe Kaymak  
    Sosyal Medya Devrimleri? / İsmail Hakkı Polat  
    Yeni Medya ve Sosyal Hareketler / Barış Engin  
    İnternet’i Geri Alalım / Koray Löker  
    Tarihin Gözleri / Işık Barış Fidaner  

###Bölüm II: Wikileaks, Gazetecilik ve Yeni Medya

    Bilgi Edinme Hakkı, Yeni Medya Düzeni ve Wikileaks / Özgür Uçkan  
    Yeni Medyanın Zaferi / Mete Çubukçu
    Açık-Kaynaklı Habercilik Asıl Şimdi! / Noyan Ayan
    Operasyonel Bir Gazetecilik Olarak Wikileaks / Başar Başaran
    Wikileaks versus Kapitalizm?.. / Gülseren Adaklı
    Wikileaks Üzerine Bir Tartışma Zemini Arayışı / Başar Başaran

###Bölüm III: Wikileaks’in Kuram ve Felsefesi

    Siyasal Hacktivizm’in Tuhaf Kökenleri / Julian Assange
    Hanoi yolu: Yol çukurları ve öngörüye dair / Julian Assange  
    İdare Yolu Olarak Tezgah / Julian Assange  
    Assange’ın Felsefesi ve Tezgah Kuramı / Işık Barış Fidaner  
    Wikileaks’den Sızanlar, İnternet Teknolojisi ve İktidar / Burçe Çelik  
    Wikileaks’in politikası / Andreas Müllerleile  

###Ekler

    Yazarlar Hakkında
    Leak siteleri
